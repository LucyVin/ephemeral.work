from lib.enums import BaseEnum


class Actions(BaseEnum):
    post = ()
    delete = ()
    like = ()
    platform = ()
